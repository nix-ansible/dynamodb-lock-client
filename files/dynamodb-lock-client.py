#!/usr/bin/env python

import argparse
import logging
import os
import sys
import time
import uuid

import boto3
import botocore
import boto3.dynamodb.conditions as conditions


DEFAULT_TTL = 15
LOGGING_LEVEL = logging.INFO
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
LOCK_SCHEMA = {'id': 'LockID', 'uuid': 'UUID', 'ttl': 'TTL'}

def acquire_lock(lock_id, lock_ttl=15, lock_uuid=str(uuid.uuid4()), condition_expression=conditions.Attr(LOCK_SCHEMA['id']).not_exists()):
    while not (put_lock(lock_id, lock_ttl, lock_uuid, condition_expression)):
        response = table.get_item(Key={LOCK_SCHEMA['id']: lock_id})

        if 'Item' in response:
            lock_item = response['Item']
        else:
            continue

        sleep_time = int(lock_item.get(LOCK_SCHEMA['ttl'], 1))

        if LOCK_SCHEMA['uuid'] in lock_item:
            condition_expression = conditions.Attr(LOCK_SCHEMA['uuid']).eq(lock_item[LOCK_SCHEMA['uuid']])
        else:
            condition_expression = conditions.Attr(LOCK_SCHEMA['uuid']).not_exists()

        logging.info("Waiting on lock ttl to expire: {0}s".format(response["Item"][LOCK_SCHEMA['ttl']]))
        time.sleep(sleep_time)

    response = table.get_item(Key={LOCK_SCHEMA['id']: lock_id})
    return response['Item'][LOCK_SCHEMA['uuid']]


def put_lock(lock_id, lock_ttl=15, lock_uuid=str(uuid.uuid4()), cond=conditions.Attr(LOCK_SCHEMA['id']).not_exists()):
    logging.debug("Lock UUID: {}".format(lock_uuid))

    try:
        response = table.put_item(
            Item={LOCK_SCHEMA['id']: lock_id, LOCK_SCHEMA['uuid']: lock_uuid, LOCK_SCHEMA['ttl']: lock_ttl},
            ConditionExpression=cond
        )
    except botocore.exceptions.ClientError as e:
        # Ignore the ConditionalCheckFailedException, bubble up
        # other exceptions.
        if e.response['Error']['Code'] != 'ConditionalCheckFailedException':
            raise
        else:
            response = table.get_item(Key={LOCK_SCHEMA['id']: lock_id})
            logging.info('Lock already exists: {0}'.format(response['Item']))
            return False

    response = table.get_item(Key={LOCK_SCHEMA['id']: lock_id})
    logging.info("Successfully acquired lock: {0}".format(response['Item']))

    return response['Item']


def lock_hearbeat(lock_id, lock_ttl, hearbeat_ttl, lock_uuid):
    logging.info("Renewing lock every {0}s".format(hearbeat_ttl))

    while True:
        time.sleep(hearbeat_ttl)

        lock_uuid_new = str(uuid.uuid4())

        response = table.put_item(
            Item={LOCK_SCHEMA['id']: lock_id, LOCK_SCHEMA['uuid']: lock_uuid_new, LOCK_SCHEMA['ttl']: lock_ttl},
            ConditionExpression=conditions.Attr(LOCK_SCHEMA['uuid']).eq(lock_uuid)
        )

        response = table.get_item(Key={LOCK_SCHEMA['id']: lock_id})
        logging.info("Successfully renwing lock: {0}".format(response['Item']))

        lock_uuid = response['Item'][LOCK_SCHEMA['uuid']]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Acquire distributed lock through AWS DynamoDB')

    parser.add_argument(
        "--name", required=True,
        help="Lock name"
    )

    parser.add_argument(
        "--table", required=True,
        help="AWS DynamoDB table name"
    )

    parser.add_argument(
        "--ttl", default=DEFAULT_TTL,
        help="Lock TTL"
    )

    parser.add_argument(
        "--debug",
        dest="logging_level",
        action='store_const',
        const=logging.DEBUG,
        default=LOGGING_LEVEL,
        help="Turn on verbose output"
    )

    parser.add_argument(
        "--deamon",
        action='store_const',
        const=True,
        default=False,
        help="Keep lock forever, bt renewing it every 5s"
    )

    args = parser.parse_args()

    logging.basicConfig(
        stream=sys.stdout,
        level=args.logging_level,
        format='%(levelname)s: %(message)s'
    )

    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(args.table)
    lock_id = args.name
    condition_expression = conditions.Attr(LOCK_SCHEMA['id']).not_exists()
    lock_ttl = args.ttl
    lock_uuid=str(uuid.uuid4())
    keep_lock_forever = args.deamon

    lock_uuid = acquire_lock(lock_id, lock_ttl, lock_uuid, condition_expression)

    if keep_lock_forever:
        lock_hearbeat(lock_id, lock_ttl, 5, lock_uuid)
